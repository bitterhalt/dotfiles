-------------------------------------------------
-- RRethy/nvim-base16 collection
-------------------------------------------------

-- local ok, _ = pcall(vim.cmd, 'colorscheme base16-dracula')
-- local ok, _ = pcall(vim.cmd, 'colorscheme base16-monokai')
-- local ok, _ = pcall(vim.cmd, 'colorscheme base16-nord')
-- local ok, _ = pcall(vim.cmd, 'colorscheme base16-oceanicnext')
-- local ok, _ = pcall(vim.cmd, "colorscheme base16-onedark")
-- local ok, _ = pcall(vim.cmd, 'colorscheme palenight')
-- local ok, _ = pcall(vim.cmd, 'colorscheme base16-solarized-dark')
-- local ok, _ = pcall(vim.cmd, 'colorscheme base16-solarized-light')
-- local ok, _ = pcall(vim.cmd, 'colorscheme base16-gruvbox-dark-hard')

-- Custon Gruvbox Darker
require('base16-colorscheme').setup({
     base00 = '#1b1b1b', base01 = '#262626', base02 = '#282828', base03 = '#545454',
     base04 = '#afafaf', base05 = '#c6c6c6', base06 = '#e8e8e8', base07 = '#ffffff',
     base08 = '#fb4934', base09 = '#fe8019', base0A = '#fabd2f', base0B = '#b8bb26',
     base0C = '#8ec07c', base0D = '#458588', base0E = '#d3869b', base0F = '#d65d0e'
    })




